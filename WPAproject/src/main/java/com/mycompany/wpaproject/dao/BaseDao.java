/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 *
 * @author KUBA
 */

public abstract class BaseDao<T,ID> implements GenericDao<T,ID>{
    private final Class<T> type;

    @PersistenceContext
    protected EntityManager em;

    protected BaseDao(Class<T> type) {
        this.type = type;
    }

    @Override
    public List<T> findAll() {
        return em.createQuery("SELECT e FROM " + type.getName() + " e", type).getResultList();
    }

    @Override
    public T find(ID id) {
        return em.find(type, id);
    }

    @Override
    public Long getCount() {
        return em.createQuery("SELECT count(e) FROM " + type.getName() + " e", Long.class).getSingleResult();
    }

    @Override
    public void remove(T instance) {
        em.remove(instance);
    }

    @Override
    public void persist(T instance) {
        em.persist(instance);
    }

    @Override
    public void update(T instance) {
        em.merge(instance);
    }
    
    @Override
    public T loadById(ID id) {
        return em.getReference(type, id);
    }

}
