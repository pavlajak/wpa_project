/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.dao;

import com.mycompany.wpaproject.bo.Book;
import com.mycompany.wpaproject.bo.BookId;
import com.mycompany.wpaproject.bo.User;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author KUBA
 */
@Component
public class BookDao extends BaseDao<Book, BookId>{
    
    public BookDao() {
        super(Book.class);
    }
    
    public List<Book> getBooksByUser(User user) {
        return em.createQuery("SELECT b FROM Book b WHERE b.holder = :holder", Book.class).setParameter("holder", user).getResultList();
    }
  
}
