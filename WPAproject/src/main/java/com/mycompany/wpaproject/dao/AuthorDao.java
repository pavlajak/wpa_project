/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.dao;

import com.mycompany.wpaproject.bo.Author;
import com.mycompany.wpaproject.bo.Book;
import java.util.List;

/**
 *
 * @author KUBA
 */
public class AuthorDao extends BaseDao{

    public AuthorDao() {
        super(Author.class);
    }
    
    public List<Book> getBooksByAuthor(Author author) {
        return em.createQuery("SELECT b FROM Book b WHERE b.author = :author", Book.class).setParameter("author", author).getResultList();
    }
    
}
