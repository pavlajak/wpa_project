/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.dao;

import com.mycompany.wpaproject.bo.Book;
import com.mycompany.wpaproject.bo.Review;
import com.mycompany.wpaproject.bo.User;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author KUBA
 */
@Component
public class ReviewDao extends BaseDao<Review, Long>{
    
    public ReviewDao() {
        super(Review.class);
    }
    
    public List<Review> getReviewByUser(User user){
        return em.createQuery("SELECT r FROM Review r WHERE r.author = :author", Review.class).setParameter("author", user).getResultList();
    }
    
    public List<Review> getReviewByBook(Book book){
        return em.createQuery("SELECT r FROM Review r WHERE r.describes = :describes", Review.class).setParameter("describes", book).getResultList();
    }
    
}
