/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.dao;

import com.mycompany.wpaproject.bo.User;
import org.springframework.stereotype.Component;

/**
 *
 * @author KUBA
 */
@Component
public class UserDao extends BaseDao<User, Long>{
    
    public UserDao(){
        super(User.class);
    }
    
    
}
