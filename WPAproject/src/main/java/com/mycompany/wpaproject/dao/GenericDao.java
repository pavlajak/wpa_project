/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.dao;

import java.util.List;

/**
 *
 * @author KUBA
 */
public interface GenericDao<T, ID> {
    public List<T> findAll();
    public T find(ID id);
    public Long getCount();
    public void remove(T instance);
    public void persist(T instance);
    public void update(T instance);  
    public T loadById(ID id);
}