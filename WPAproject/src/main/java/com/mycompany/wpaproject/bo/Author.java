/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author KUBA
 */
@Entity
public class Author implements Serializable{
    
    @Id
    @GeneratedValue(generator="system-sequence")
    private Long id;
    
    @Column
    String name;
    
    @OneToMany(mappedBy="author", cascade=CascadeType.REMOVE)
    private List<Book> books;
    
    public Author(){}
    
    
    public void addBook(Book book){
        if(this.books == null){
            books = new ArrayList<>();
        }
        if(!this.books.contains(book)){
            books.add(book);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
    
    
}
