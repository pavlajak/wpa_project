/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.bo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author KUBA
 */
@Entity
public class Review implements Serializable {
    
    @Id
    @GeneratedValue(generator="system-sequence")
    protected Long id;
    
    @Column
    String review;
    
    @Column
    byte stars;
    
    @ManyToOne
    private User author;
    
    @ManyToOne
    private Book describes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public byte getStars() {
        return stars;
    }

    public void setStars(byte stars) {
        this.stars = stars;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Book getDescribes() {
        return describes;
    }

    public void setDescribes(Book describes) {
        this.describes = describes;
    }
        
}
