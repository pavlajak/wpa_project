/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.bo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author KUBA
 */

@Embeddable
public class BookId implements Serializable {
    
    static int serialNumber = 0;
    
    @Column(name="ISBN", nullable=false)
    private String isbn;
    
    @Column(name="serial")
    private int serial;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {        
        this.serial = serial;        
    }
    
    public int requestSerial(){
        return serialNumber++;
    }

}
