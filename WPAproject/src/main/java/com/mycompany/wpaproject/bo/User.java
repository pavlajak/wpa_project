/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wpaproject.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author KUBA
 */

@MappedSuperclass
public class User {
    
    @Id
    @GeneratedValue(generator="system-sequence")
    private Long id;
    
    @Column
    private String username;
    
    @Column
    private String password;

    @OneToMany(mappedBy="holder", cascade=CascadeType.REMOVE)
    private List<Book> books;    
    
    @OneToMany(mappedBy="author")
    private List<Review> reviews;
    
    @Column
    @OneToOne
    Badge badge;
    
    public User(){}
    
    public User(String username, String password){
        this.username = username;
        this.password = password;    
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Badge getBadge() {
        return badge;
    }

    public void setBadge(Badge badge) {
        this.badge = badge;
    }
    
    public void addBook(Book book){
        if(this.books == null){
            books = new ArrayList<>();
        }
        if(!this.books.contains(book)){
            books.add(book);
        }
        book.setDate(new Date(System.currentTimeMillis()));
    }  
    
    public void addBadge(Badge badge){
        this.badge = badge;        
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }
    
    
    public Book returnBook(Book book){
        if(this.books.contains(book)) {
           Book tmp = book;
           this.books.remove(book);
           return tmp;
        }
        throw new IllegalArgumentException("User " + getUsername() + " does not hold book " + book.getTitle() + ".");    
    }
    
    public void addReview(Review review){
        if(reviews == null){
            reviews = new ArrayList<>();        
        }
        if(!reviews.contains(review)){
            reviews.add(review);        
        }
    }

}