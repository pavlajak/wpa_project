/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycopmany.wpaproject.service;

import com.mycompany.wpaproject.dao.GenericDao;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author KUBA
 */
public abstract class AbstractDataAccessService<T, ID> implements DataAccessService<T, ID>{
    
    protected abstract GenericDao<T, ID> getPrimaryDao();

    @Transactional(readOnly = true)
    @Override
    public List<T> findAll() {
        return getPrimaryDao().findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public T find(ID id) {
        return getPrimaryDao().find(id);
    }

    @Transactional
    @Override
    public void persist(T instance) {
        getPrimaryDao().persist(instance);
    }

    @Transactional
    @Override
    public void update(T instance) {
        getPrimaryDao().update(instance);
    }

    @Transactional
    @Override
    public void remove(T instance) {
        getPrimaryDao().remove(instance);
    }

    @Transactional
    @Override
    public void removeById(ID id) {
        final T toRemove = getPrimaryDao().loadById(id);
        if (toRemove != null) {
            getPrimaryDao().remove(toRemove);
        }
    }
}
