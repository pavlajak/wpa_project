/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycopmany.wpaproject.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author KUBA
 */


public interface DataAccessService<T,ID> {
    @Transactional(readOnly = true)
    T find(ID id);

    @Transactional(readOnly = true)
    List<T> findAll();

    @Transactional
    void persist(T instance);

    @Transactional
    void remove(T instance);

    @Transactional
    void removeById(ID id);

    @Transactional
    void update(T instance);
}
