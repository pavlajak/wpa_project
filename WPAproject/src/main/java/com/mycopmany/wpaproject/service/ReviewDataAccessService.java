/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycopmany.wpaproject.service;

import com.mycompany.wpaproject.bo.Review;
import com.mycompany.wpaproject.dao.GenericDao;
import com.mycompany.wpaproject.dao.ReviewDao;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author KUBA
 */
public class ReviewDataAccessService extends AbstractDataAccessService<Review, Long>{

    @Autowired
    private ReviewDao reviewDao;
    
    @Override
    protected GenericDao<Review, Long> getPrimaryDao() {
        return reviewDao;
    }
    
    public Long addReview(String review, byte stars){
        Review newReview = new Review();
        newReview.setReview(review);
        if(stars < 11 && stars > -1) newReview.setStars(stars);
        else throw new IllegalArgumentException("Stars must be between 0 and 10!");
    
        persist(newReview);
        
        return newReview.getId();
    }
}
