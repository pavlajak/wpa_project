/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycopmany.wpaproject.service;

import com.mycompany.wpaproject.bo.Author;
import com.mycompany.wpaproject.bo.Book;
import com.mycompany.wpaproject.bo.BookId;
import com.mycompany.wpaproject.bo.User;
import com.mycompany.wpaproject.dao.BookDao;
import com.mycompany.wpaproject.dao.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author KUBA
 */
@Service
public class BookDataAccessService extends AbstractDataAccessService<Book, BookId>{

    @Autowired
    private BookDao bookDao;
    
    
    @Override
    protected GenericDao<Book, BookId> getPrimaryDao() {
        return bookDao;
    }
    
    public BookId addBook(String title, String ISBN, Author author){
        Book newBook = new Book();
        newBook.setTitle(title);
        author.addBook(newBook);
        newBook.setAuthor(author);
        BookId newBookId = new BookId();
        newBookId.setIsbn(ISBN);
        newBookId.setSerial(newBookId.requestSerial());
        
        newBook.setId(newBookId);
        
        persist(newBook);
        //do I need to persist BookId?
        
        return newBook.getId();
    }
    
    public void borrowBook(Book book, User user){
        user.addBook(book);    
    }
    
}
