/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycopmany.wpaproject.service;

import com.mycompany.wpaproject.bo.Author;
import com.mycompany.wpaproject.dao.AuthorDao;
import com.mycompany.wpaproject.dao.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author KUBA
 */
@Service
public class AuthorDataAccessService extends AbstractDataAccessService<Author, Long>{

    @Autowired
    private AuthorDao authorDao;
    
    @Override
    protected GenericDao<Author, Long> getPrimaryDao() {
        return authorDao;
    }
    
    public Long addAuthor(String name){
        Author newAuthor = new Author();
        newAuthor.setName(name);
        
        persist(newAuthor);
        
        return newAuthor.getId();
    
    }
}
