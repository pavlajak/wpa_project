/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycopmany.wpaproject.service;

import com.mycompany.wpaproject.bo.Admin;
import com.mycompany.wpaproject.bo.Person;
import com.mycompany.wpaproject.bo.User;
import com.mycompany.wpaproject.dao.GenericDao;
import com.mycompany.wpaproject.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author KUBA
 */
@Service
public class UserDataAccessService extends AbstractDataAccessService<User, Long>{

    
    @Autowired
    private UserDao userDao;
    
    @Override
    protected GenericDao<User, Long> getPrimaryDao() {
        return userDao;
    }
    
    public Long addUser(String username, String password){
        Person newUser = new Person();
        newUser.setUsername(username);
        newUser.setPassword(password);
        
        persist(newUser);
        
        return newUser.getId();    
    }
    
    public Long addAdmin(String usernae, String password){
        Admin newUser = new Admin();
        newUser.setUsername(usernae);
        newUser.setPassword(password);
        
        persist(newUser);
        
        return newUser.getId();    
    }
    
}
