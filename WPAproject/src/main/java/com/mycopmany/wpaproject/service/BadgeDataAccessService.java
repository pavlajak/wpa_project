/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycopmany.wpaproject.service;

import com.mycompany.wpaproject.bo.Badge;
import com.mycompany.wpaproject.dao.BadgeDao;
import com.mycompany.wpaproject.dao.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author KUBA
 */
@Service
public class BadgeDataAccessService extends AbstractDataAccessService<Badge, Long>{

    @Autowired
    private BadgeDao badgeDao;
    
    @Override
    protected GenericDao<Badge, Long> getPrimaryDao() {
        return badgeDao;
    }
    
}
